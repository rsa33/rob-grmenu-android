package net.srcf.rsa33.grmenu;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;

public class MainActivity extends ActionBarActivity {

    // Downloaded menus
    private Map<String, List<String[]>> mMenus;

    // List view and empty text
    private ExpandableListView mExplistMain;
    private TextView mTextEmpty;
    
    // Refresh animation menu item
    private boolean mRefreshing = false;
    private boolean mRefreshProgress = false;
    private MenuItem mRefreshItem;
    private ImageView mRefreshImage;

    private LayoutInflater mInflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mExplistMain = (ExpandableListView) findViewById(R.id.explist_main);
        mTextEmpty = (TextView) mInflater.inflate(R.layout.explist_empty, null);
        addContentView(mTextEmpty, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mExplistMain.setEmptyView(mTextEmpty);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMenus != null) {
            final String[] groups = mMenus.keySet().toArray(new String[mMenus.keySet().size()]);
            mExplistMain.setAdapter(new ExpListAdapter(this, groups, mMenus));
            mExplistMain.expandGroup(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mRefreshItem = (MenuItem) menu.getItem(0);
        mRefreshImage = (ImageView) mInflater.inflate(R.layout.action_refresh, null);
        if (mMenus == null) {
            refresh();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu_refresh:
                refresh();
                break;
            case R.id.main_menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            default:
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    // Download and update the list of menus
    public void refresh() {
        if (!mRefreshProgress) {
            showProgress();
        }
        if (!mRefreshing) {
            mRefreshing = true;
            JSONFetch parser = new JSONFetch();
            parser.execute(new JSONFetch.FetchHandler(this) {
                @Override
                public void onComplete(JSONObject jRoot) {
                    try {
                        // Parse JSON response
                        Map<String, List<String[]>> menus = new LinkedHashMap<String, List<String[]>>();
                        JSONArray jMenus = jRoot.getJSONArray("menus");
                        for (int i = 0; i < jMenus.length(); i++) {
                            JSONObject jMenu = jMenus.getJSONObject(i);
                            String date = jMenu.getString("date");
                            List<String[]> items = new ArrayList<String[]>();
                            if (jMenu.has("lunch")) {
                                items.addAll(parseMenuItems(jMenu.getJSONArray("lunch"), "lunch"));
                            }
                            if (jMenu.has("dinner")) {
                                items.addAll(parseMenuItems(jMenu.getJSONArray("dinner"), "dinner"));
                            }
                            if (items.size() > 0) {
                                menus.put(date, items);
                            }
                        }
                        // Update UI
                        mMenus = menus;
                        onResume();
                    } catch (JSONException e) {
                        if (mRefreshProgress) {
                            mTextEmpty.setText("Unable to read the menus.  Please try again later.");
                        }
                    }
                    hideProgress();
                    mRefreshing = false;
                }
                @Override
                public void onError(int status) {
                    if (mRefreshProgress) {
                        String message = "Unable to download the menus.  Please try again later.";
                        if (status == JSONFetch.STATUS_NO_CONNECTION) {
                            message = "Unable to connect to the internet.  Please check your connection and try again.";
                        } else if (status == JSONFetch.STATUS_PARSE_ERROR) {
                            message = "Unable to read the menus.  Please try again later.";
                        }
                        mTextEmpty.setText(message);
                        hideProgress();
                    }
                    mRefreshing = false;
                }
            });
        }
    }

    public void showProgress() {
        if (mRefreshImage != null) {
            mRefreshProgress = true;
            mTextEmpty.setText("Refreshing...");
            Animation rotation = AnimationUtils.loadAnimation(this, R.drawable.spin);
            rotation.setRepeatCount(Animation.INFINITE);
            mRefreshImage.startAnimation(rotation);
            MenuItemCompat.setActionView(mRefreshItem, mRefreshImage);
        }
    }

    public void hideProgress() {
        if (mRefreshImage != null) {
            mRefreshImage.clearAnimation();
            MenuItemCompat.setActionView(mRefreshItem, null);
            mRefreshProgress = false;
        }
    }

    // Parse a list of menu items
    public List<String[]> parseMenuItems(JSONArray jItems, String type) throws JSONException {
        List<String[]> items = new ArrayList<String[]>();
        items.add(new String[]{type});
        for (int j = 0; j < jItems.length(); j++) {
            JSONObject jItem = jItems.getJSONObject(j);
            String item = jItem.getString("item");
            JSONArray jFlags = jItem.getJSONArray("flags");
            String flags = "";
            for (int i = 0; i < jFlags.length(); i++) {
                flags += jFlags.getString(i) + "  ";
            }
            if (flags.length() > 0) {
                flags = flags.substring(0, flags.length() - 2);
            }
            if (jItem.has("price")) {
                JSONArray jPrice = jItem.getJSONArray("price");
                String price = String.format(Locale.getDefault(), "%.2f", Float.valueOf(jPrice.getString(0)));
                if (jPrice.length() == 2) {
                    String altPrice = String.format(Locale.getDefault(), "%.2f", Float.valueOf(jPrice.getString(1)));
                    items.add(new String[]{item, flags, price, altPrice});
                } else {
                    items.add(new String[]{item, flags, price});
                }
            } else {
                items.add(new String[]{item, flags});
            }
        }
        return items;
    }

}
