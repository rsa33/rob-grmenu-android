package net.srcf.rsa33.grmenu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
 
public class ExpListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    // Group header names
    private String[] mGroups;
    // Map of header to data
    private Map<String, List<String[]>> mMenus;
    private SharedPreferences mPrefs;

    public ExpListAdapter(Context context, String[] headers, Map<String, List<String[]>> menus) {
        mContext = context;
        mInflater = (LayoutInflater) mContext .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mGroups = headers;
        mMenus = menus;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public String[] getChild(int groupPosition, int childPosititon) {
        return mMenus.get(mGroups[groupPosition]).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String[] item = getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.explist_item, null);
        }
        LinearLayout layItemMain = (LinearLayout) convertView.findViewById(R.id.lay_item_main);
        TextView textItemMain = (TextView) convertView.findViewById(R.id.text_item_item);
        LinearLayout layItemMeta = (LinearLayout) convertView.findViewById(R.id.lay_item_meta);
        TextView textItemFlags = (TextView) convertView.findViewById(R.id.text_item_flags);
        TextView textItemPrice = (TextView) convertView.findViewById(R.id.text_item_price);
        TextView textItemAltPrice = (TextView) convertView.findViewById(R.id.text_item_altPrice);
        if (item.length == 1) {
            layItemMain.setBackgroundColor(Color.rgb(224, 224, 224));
            textItemMain.setTypeface(null, Typeface.BOLD);
            textItemMain.setText(item[0].toUpperCase(Locale.getDefault()));
            layItemMeta.setVisibility(View.GONE);
            textItemFlags.setText("");
            textItemPrice.setText("");
        } else {
            layItemMain.setBackgroundColor(Color.rgb(255, 255, 255));
            textItemMain.setTypeface(null, Typeface.NORMAL);
            textItemMain.setText(item[0]);
            layItemMeta.setVisibility(View.VISIBLE);
            if (mPrefs.getBoolean("flags", true)) {
                textItemFlags.setVisibility(View.VISIBLE);
                textItemFlags.setText(item[1]);
            } else {
                textItemFlags.setVisibility(View.GONE);
                textItemFlags.setText("");
            }
            if (item.length > 2 && mPrefs.getBoolean("prices", true)) {
                textItemPrice.setVisibility(View.VISIBLE);
                textItemPrice.setText("�" + item[2]);
                if (item.length > 3 && mPrefs.getBoolean("altPrice", true)) {
                    textItemAltPrice.setVisibility(View.VISIBLE);
                    textItemAltPrice.setText("(�" + item[3] + ")");
                } else {
                    textItemAltPrice.setVisibility(View.GONE);
                    textItemAltPrice.setText("");
                }
            } else {
                textItemPrice.setVisibility(View.GONE);
                textItemPrice.setText("");
                textItemAltPrice.setVisibility(View.GONE);
                textItemAltPrice.setText("");
            }
        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mMenus.get(mGroups[groupPosition]).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mGroups[groupPosition];
    }

    @Override
    public int getGroupCount() {
        return mGroups.length;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String date = (String) getGroup(groupPosition);
        String day = date;
        try {
            Date parsed = mDateFormat.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsed);
            Calendar now = Calendar.getInstance();
            if (cal.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR)) {
                day = "TODAY";
            } else if (cal.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR) + 1) {
                day = "TOMORROW";
            } else {
                String[] days = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
                day = days[cal.get(Calendar.DAY_OF_WEEK) - 1];
            }
            cal.get(Calendar.DAY_OF_WEEK);
        } catch (ParseException e) {}
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.explist_group, null);
        }
        TextView textHeadDay = (TextView) convertView.findViewById(R.id.text_group_day);
        textHeadDay.setText(day);
        TextView textHeadDate = (TextView) convertView.findViewById(R.id.text_group_date);
        textHeadDate.setText(date);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
