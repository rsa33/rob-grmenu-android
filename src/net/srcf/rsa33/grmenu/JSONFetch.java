package net.srcf.rsa33.grmenu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

public class JSONFetch extends AsyncTask<JSONFetch.FetchHandler, Void, JSONObject> {

    // Status enums
    public static final int STATUS_FAILED = 1;
    public static final int STATUS_NO_CONNECTION = 2;
    public static final int STATUS_PARSE_ERROR = 3;

    // Progress and handlers
    private int mStatus = 0;
    private FetchHandler[] mHandlers;

    // Download and parse a JSON file, return the root node array
    @Override
    protected JSONObject doInBackground(FetchHandler... handlers) {
        mHandlers = handlers;
        JSONObject jObjRoot = null;
        InputStream stream = null;
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            String limit = PreferenceManager.getDefaultSharedPreferences(mHandlers[0].getContext()).getString("limit", "7");
            String domain = mHandlers[0].getContext().getString(R.string.url_api) + "?lim=" + limit;
            HttpResponse httpResponse = httpClient.execute(new HttpGet(domain));
            stream = httpResponse.getEntity().getContent();
        } catch (IOException e) {
            e.printStackTrace();
            mStatus = STATUS_NO_CONNECTION;
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            stream.close();
            String result = sb.toString();
            jObjRoot = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
            mStatus = STATUS_PARSE_ERROR;
        } catch (NullPointerException e) {
            e.printStackTrace();
            mStatus = STATUS_NO_CONNECTION;
        } catch (Exception e) {
            e.printStackTrace();
            mStatus = STATUS_FAILED;
        }
        return jObjRoot;
    }

    // Run the given callback function on completion
    @Override
    protected void onPostExecute(JSONObject resp) {
        for (FetchHandler handler : mHandlers) {
            if (mStatus > 0) {
                handler.onError(mStatus);
            } else {
                handler.onComplete(resp);
            }
        }
    }

    public static abstract class FetchHandler {

        private Context mContext;

        public FetchHandler(Context context) {
            mContext = context;
        }

        public Context getContext() {
            return mContext;
        }

        public abstract void onComplete(JSONObject resp);
        public abstract void onError(int status);

    }

}
